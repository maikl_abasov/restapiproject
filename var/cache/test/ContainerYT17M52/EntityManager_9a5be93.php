<?php

namespace ContainerYT17M52;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'src'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder2269a = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer899e0 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties64e5d = [
        
    ];

    public function getConnection()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getConnection', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getMetadataFactory', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getExpressionBuilder', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'beginTransaction', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getCache', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getCache();
    }

    public function transactional($func)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'transactional', array('func' => $func), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'wrapInTransaction', array('func' => $func), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'commit', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->commit();
    }

    public function rollback()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'rollback', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getClassMetadata', array('className' => $className), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'createQuery', array('dql' => $dql), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'createNamedQuery', array('name' => $name), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'createQueryBuilder', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'flush', array('entity' => $entity), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'clear', array('entityName' => $entityName), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->clear($entityName);
    }

    public function close()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'close', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->close();
    }

    public function persist($entity)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'persist', array('entity' => $entity), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'remove', array('entity' => $entity), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'refresh', array('entity' => $entity), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'detach', array('entity' => $entity), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'merge', array('entity' => $entity), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getRepository', array('entityName' => $entityName), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'contains', array('entity' => $entity), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getEventManager', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getConfiguration', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'isOpen', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getUnitOfWork', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getProxyFactory', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'initializeObject', array('obj' => $obj), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'getFilters', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'isFiltersStateClean', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'hasFilters', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return $this->valueHolder2269a->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer899e0 = $initializer;

        return $instance;
    }

    public function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config)
    {
        static $reflection;

        if (! $this->valueHolder2269a) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder2269a = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder2269a->__construct($conn, $config);
    }

    public function & __get($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__get', ['name' => $name], $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        if (isset(self::$publicProperties64e5d[$name])) {
            return $this->valueHolder2269a->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__isset', array('name' => $name), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__unset', array('name' => $name), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__clone', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $this->valueHolder2269a = clone $this->valueHolder2269a;
    }

    public function __sleep()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__sleep', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return array('valueHolder2269a');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer899e0 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer899e0;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'initializeProxy', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder2269a;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder2269a;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
