<?php

namespace ContainerCveYHvY;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'knplabs'.\DIRECTORY_SEPARATOR.'knp-components'.\DIRECTORY_SEPARATOR.'src'.\DIRECTORY_SEPARATOR.'Knp'.\DIRECTORY_SEPARATOR.'Component'.\DIRECTORY_SEPARATOR.'Pager'.\DIRECTORY_SEPARATOR.'PaginatorInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'knplabs'.\DIRECTORY_SEPARATOR.'knp-components'.\DIRECTORY_SEPARATOR.'src'.\DIRECTORY_SEPARATOR.'Knp'.\DIRECTORY_SEPARATOR.'Component'.\DIRECTORY_SEPARATOR.'Pager'.\DIRECTORY_SEPARATOR.'Paginator.php';

class PaginatorInterface_82dac15 implements \ProxyManager\Proxy\VirtualProxyInterface, \Knp\Component\Pager\PaginatorInterface
{
    /**
     * @var \Knp\Component\Pager\PaginatorInterface|null wrapped object, if the proxy is initialized
     */
    private $valueHolder2269a = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer899e0 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties64e5d = [
        
    ];

    public function paginate($target, int $page = 1, ?int $limit = null, array $options = []) : \Knp\Component\Pager\Pagination\PaginationInterface
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'paginate', array('target' => $target, 'page' => $page, 'limit' => $limit, 'options' => $options), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        if ($this->valueHolder2269a === $returnValue = $this->valueHolder2269a->paginate($target, $page, $limit, $options)) {
            return $this;
        }

        return $returnValue;
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        $instance->initializer899e0 = $initializer;

        return $instance;
    }

    public function __construct()
    {
        static $reflection;

        if (! $this->valueHolder2269a) {
            $reflection = $reflection ?? new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
            $this->valueHolder2269a = $reflection->newInstanceWithoutConstructor();
        }
    }

    public function & __get($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__get', ['name' => $name], $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        if (isset(self::$publicProperties64e5d[$name])) {
            return $this->valueHolder2269a->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__isset', array('name' => $name), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__unset', array('name' => $name), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2269a;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder2269a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__clone', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        $this->valueHolder2269a = clone $this->valueHolder2269a;
    }

    public function __sleep()
    {
        $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, '__sleep', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;

        return array('valueHolder2269a');
    }

    public function __wakeup()
    {
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer899e0 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer899e0;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer899e0 && ($this->initializer899e0->__invoke($valueHolder2269a, $this, 'initializeProxy', array(), $this->initializer899e0) || 1) && $this->valueHolder2269a = $valueHolder2269a;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder2269a;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder2269a;
    }
}

if (!\class_exists('PaginatorInterface_82dac15', false)) {
    \class_alias(__NAMESPACE__.'\\PaginatorInterface_82dac15', 'PaginatorInterface_82dac15', false);
}
