<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/customer/add' => [[['_route' => 'add_customer', '_controller' => 'App\\Controller\\CustomerController::addCustomer'], null, ['POST' => 0], null, false, false, null]],
        '/customer/get-all' => [[['_route' => 'get_all_customers', '_controller' => 'App\\Controller\\CustomerController::getAllCustomers'], null, ['GET' => 0], null, false, false, null]],
        '/profile/list/all' => [[['_route' => 'profiles_all', '_controller' => 'App\\Controller\\ProfileController::getList'], null, null, null, false, false, null]],
        '/profile/create' => [[['_route' => 'create_profile', '_controller' => 'App\\Controller\\ProfileController::create'], null, null, null, false, false, null]],
        '/user/list/all' => [[['_route' => 'users_all', '_controller' => 'App\\Controller\\UserController::getList'], null, null, null, false, false, null]],
        '/user/login' => [[['_route' => 'login_user', '_controller' => 'App\\Controller\\UserController::login'], null, null, null, false, false, null]],
        '/user/create' => [[['_route' => 'create_user', '_controller' => 'App\\Controller\\UserController::create'], null, null, null, false, false, null]],
        '/user/my-custom-tests-start' => [[['_route' => 'custom_tests', '_controller' => 'App\\Controller\\UserController::myCustomTestsStart'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/customer/(?'
                    .'|get/([^/]++)(*:32)'
                    .'|update/([^/]++)(*:54)'
                    .'|delete/([^/]++)(*:76)'
                .')'
                .'|/profile/(?'
                    .'|item(?'
                        .'|/([^/]++)(?:/([^/]++))?(*:126)'
                        .'|s/([^/]++)(?:/([^/]++))?(*:158)'
                    .')'
                    .'|delete/([^/]++)(*:182)'
                .')'
                .'|/user/(?'
                    .'|item(?'
                        .'|/([^/]++)(?:/([^/]++))?(*:230)'
                        .'|s/([^/]++)(?:/([^/]++))?(*:262)'
                    .')'
                    .'|delete/([^/]++)(*:286)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        32 => [[['_route' => 'get_one_customer', '_controller' => 'App\\Controller\\CustomerController::getOneCustomer'], ['id'], ['GET' => 0], null, false, true, null]],
        54 => [[['_route' => 'update_customer', '_controller' => 'App\\Controller\\CustomerController::updateCustomer'], ['id'], ['PUT' => 0], null, false, true, null]],
        76 => [[['_route' => 'delete_customer', '_controller' => 'App\\Controller\\CustomerController::deleteCustomer'], ['id'], ['DELETE' => 0], null, false, true, null]],
        126 => [[['_route' => 'get_profile', 'fname' => 'id', '_controller' => 'App\\Controller\\ProfileController::selectItem'], ['fvalue', 'fname'], null, null, false, true, null]],
        158 => [[['_route' => 'get_profiles', 'fname' => 'id', '_controller' => 'App\\Controller\\ProfileController::selectItems'], ['fvalue', 'fname'], null, null, false, true, null]],
        182 => [[['_route' => 'delete_profile', '_controller' => 'App\\Controller\\ProfileController::remove'], ['id'], null, null, false, true, null]],
        230 => [[['_route' => 'get_user', 'fname' => 'id', '_controller' => 'App\\Controller\\UserController::selectItem'], ['fvalue', 'fname'], null, null, false, true, null]],
        262 => [[['_route' => 'get_users', 'fname' => 'id', '_controller' => 'App\\Controller\\UserController::selectItems'], ['fvalue', 'fname'], null, null, false, true, null]],
        286 => [
            [['_route' => 'delete_user', '_controller' => 'App\\Controller\\UserController::remove'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
