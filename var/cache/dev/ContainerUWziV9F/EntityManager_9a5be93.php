<?php

namespace ContainerUWziV9F;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderf3ed3 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer46648 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties46b33 = [
        
    ];

    public function getConnection()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getConnection', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getMetadataFactory', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getExpressionBuilder', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'beginTransaction', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getCache', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getCache();
    }

    public function transactional($func)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'transactional', array('func' => $func), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'wrapInTransaction', array('func' => $func), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'commit', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->commit();
    }

    public function rollback()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'rollback', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getClassMetadata', array('className' => $className), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'createQuery', array('dql' => $dql), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'createNamedQuery', array('name' => $name), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'createQueryBuilder', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'flush', array('entity' => $entity), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'clear', array('entityName' => $entityName), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->clear($entityName);
    }

    public function close()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'close', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->close();
    }

    public function persist($entity)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'persist', array('entity' => $entity), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'remove', array('entity' => $entity), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'refresh', array('entity' => $entity), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'detach', array('entity' => $entity), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'merge', array('entity' => $entity), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getRepository', array('entityName' => $entityName), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'contains', array('entity' => $entity), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getEventManager', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getConfiguration', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'isOpen', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getUnitOfWork', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getProxyFactory', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'initializeObject', array('obj' => $obj), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'getFilters', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'isFiltersStateClean', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'hasFilters', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return $this->valueHolderf3ed3->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer46648 = $initializer;

        return $instance;
    }

    public function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config)
    {
        static $reflection;

        if (! $this->valueHolderf3ed3) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderf3ed3 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderf3ed3->__construct($conn, $config);
    }

    public function & __get($name)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, '__get', ['name' => $name], $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        if (isset(self::$publicProperties46b33[$name])) {
            return $this->valueHolderf3ed3->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3ed3;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf3ed3;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3ed3;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf3ed3;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, '__isset', array('name' => $name), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3ed3;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderf3ed3;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, '__unset', array('name' => $name), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3ed3;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderf3ed3;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, '__clone', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        $this->valueHolderf3ed3 = clone $this->valueHolderf3ed3;
    }

    public function __sleep()
    {
        $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, '__sleep', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;

        return array('valueHolderf3ed3');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer46648 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer46648;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer46648 && ($this->initializer46648->__invoke($valueHolderf3ed3, $this, 'initializeProxy', array(), $this->initializer46648) || 1) && $this->valueHolderf3ed3 = $valueHolderf3ed3;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderf3ed3;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderf3ed3;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
