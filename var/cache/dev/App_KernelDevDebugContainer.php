<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerUWziV9F\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerUWziV9F/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerUWziV9F.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerUWziV9F\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerUWziV9F\App_KernelDevDebugContainer([
    'container.build_hash' => 'UWziV9F',
    'container.build_id' => '175d8844',
    'container.build_time' => 1663000959,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerUWziV9F');
