<?php

namespace Container943ftw7;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getLotControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\LotController' shared autowired service.
     *
     * @return \App\Controller\LotController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/BaseController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/LotController.php';

        $container->services['App\\Controller\\LotController'] = $instance = new \App\Controller\LotController();

        $instance->setContainer(($container->privates['.service_locator.ULEwkxg'] ?? $container->load('get_ServiceLocator_ULEwkxgService'))->withContext('App\\Controller\\LotController', $container));

        return $instance;
    }
}
