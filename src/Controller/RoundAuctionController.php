<?php

namespace App\Controller;

use App\Entity\RoundAuction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RoundAuctionController extends BaseController
{

    protected function getModel() {
        $repo = $this->getRepo(RoundAuction::class);
        return $repo;
    }

    /**
     * @Route("/round-auction/all-rounds/{auction_type}/{limit}", name="get_all_rounds_auction")
     */
    public function getAllRounds($auction_type = 0, $limit = 0): JsonResponse{
       $repo = $this->getModel();
       $items = $repo->getAllRounds($auction_type, $limit);
       return $this->response($items);
    }

    /**
     * @Route("/round-auction/create", name="create_round_auction", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {

        $data = $this->transformJsonData($request);

        $message = (empty($data['start_date'])) ? 'Не задано начало аукциона': '';
        $message = (empty($data['end_date']))    ? 'Не задано окончание аукциона' : '';
        $message = (empty($data['auction_type'])) ? 'Не задан тип аукциона'  : '';

        if($message) {
            return $this->response(0, false, $message);
        }

        $data['start_date'] = str_replace("T", " ", $data['start_date']);
        $data['end_date']   = str_replace("T", " ", $data['end_date']);

        $item = $this->getItem([
            'start_date'   => $data['start_date'],
            'end_date'     => $data['end_date'],
            'auction_type' => $data['auction_type']
        ]);

        if(!empty($item)) {
            $message = 'Такой раунд уже существует';
            return $this->response(0, false, $message);
        }

        $repository = $this->getModel();
        $result     = $repository->add($data);
        $status = ($result) ? true : false;

        $item = $this->getItem(['id' => $result]);

        return $this->response($item, true,'Новый раунд создан');
    }

}