<?php

namespace App\Controller;

use App\Entity\User;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Route(path="/user")
 */

class UserController extends BaseController
{

    protected function getModel() {
        $repo = $this->getRepo(User::class);
        return $repo;
    }

    /**
     * @Route("/list/all", name="users_all")
     */
    public function getList(): JsonResponse
    {
        $result = $this->getItems([], true, true);
        return $this->response($result);
    }

    /**
     * @Route("/item/{fvalue}/{fname}", name="get_user")
     */
    public function selectItem($fvalue, $fname = 'id'): JsonResponse
    {
        $result = $this->getItem([$fname => $fvalue]);
        return $this->response($result);
    }

    /**
     * @Route("/items/{fvalue}/{fname}", name="get_users")
     */
    public function selectItems($fvalue, $fname = 'id'): JsonResponse
    {
        $result =  $this->getItems([$fname => $fvalue]);
        return $this->response($result);
    }

    /**
     * @Route("/delete/{id}", name="delete_user")
     */
    public function remove($id): JsonResponse
    {
        $result =  $this->deleteItem(['id' => $id]);
        return $this->response($result, true, 'Пользователь удален');
    }

    /**
     * @Route("/login", name="login_user")
     */
    public function login(Request $request): JsonResponse
    {
        $data = $this->transformJsonData($request);
        $message = (empty($data['password'])) ? 'Не задан пароль': '';
        $message = (empty($data['email']))    ? 'Не задан email' : '';
        if($message) {
            return $this->response(0, false, $message);
        }

        $password = $data['password'];
        $email    = $data['email'];
        $user     = $this->getItem(['email' => $email]);
        if(empty($user)) {
            return $this->response(0, false, 'Неправильный логин или пароль');
        }

        $status = true;
        $userPassword = $user['password'];
        $verify = password_verify($password, $userPassword);

        if($verify) {
            $secretKey = $this->getSecretKey();
            $payload = [
                "user_id"  => $user['id'],
                "role"     => $user['role'],
                "exp"      => (new \DateTime())->modify("+5 minutes")->getTimestamp(),
            ];
            $token = JWT::encode($payload, $secretKey, 'HS256');
            $status = true;
        }

        return $this->response(['token' => $token, 'user' => $user], $status, 'Аутентификация прошла успешно');
    }

    /**
     * @Route("/create", name="create_user")
     */
    public function create(Request $request): JsonResponse
    {

        $data = $this->transformJsonData($request);

        $message = (empty($data['password'])) ? 'Не задан пароль': '';
        $message = (empty($data['email']))    ? 'Не задан email' : '';
        $message = (empty($data['username'])) ? 'Не задано имя'  : '';

        if($message) {
            return $this->response(0, false, $message);
        }

        $user = $this->getItem(['email' => $data['email']], true);

        if(!empty($user)) {
            $message = 'Пользователь с таким email уже существует';
            return $this->response(0, false, $message);
        }

        $data['password'] = password_hash(trim($data['password']), PASSWORD_DEFAULT);

        $repository = $this->getModel();
        $result     = $repository->add($data);
        $status = ($result) ? true : false;

        return $this->response($result, true,'Новый пользователь создан');
    }

}
