<?php

namespace App\Controller;

use App\Entity\Profile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProfileController
 * @package App\Controller
 *
 * @Route(path="/profile")
 */

class ProfileController extends BaseController
{

    protected function getModel() {
        $repo = $this->getRepo(Profile::class);
        return $repo;
    }

    /**
     * @Route("/list/all", name="profiles_all")
     */
    public function getList(): JsonResponse
    {
        $result = $this->getItems([]);
        return $this->response($result);
    }

    /**
     * @Route("/item/{fvalue}/{fname}", name="get_profile")
     */
    public function selectItem($fvalue, $fname = 'id'): JsonResponse
    {
        $result = $this->getItem([$fname => $fvalue]);
        return $this->response($result);
    }

    /**
     * @Route("/items/{fvalue}/{fname}", name="get_profiles")
     */
    public function selectItems($fvalue, $fname = 'id'): JsonResponse
    {
        $result =  $this->getItems([$fname => $fvalue]);
        return $this->response($result);
    }

    /**
     * @Route("/delete/{id}", name="delete_profile")
     */
    public function remove($id): JsonResponse
    {
        $result =  $this->deleteItem(['id' => $id]);
        return $this->response($result, true, 'Профиль удален');
    }

    /**
     * @Route("/create", name="create_profile")
     */
    public function create(Request $request): JsonResponse
    {

        $data = $this->transformJsonData($request);
        // print_r($data); die;

        $message = (empty($data['email']))    ? 'Не задан email' : '';
        $message = (empty($data['name'])) ? 'Не задано имя'  : '';
        $message = (empty($data['user_id'])) ? 'Не задано id пользователя'  : '';
        $message = (empty($data['type'])) ? 'Не выбран тип профиля'  : '';

        if($message) {
            return $this->response(0, false, $message);
        }

        $user = $this->getItem(['email' => $data['email']], true);

        if(!empty($user)) {
            $message = 'Профиль с таким email уже существует';
            return $this->response(0, false, $message);
        }

        $repository = $this->getModel();
        $result     = $repository->add($data);
        $status = ($result) ? true : false;

        return $this->response($result, true, 'Новый профиль создан');
    }
}
