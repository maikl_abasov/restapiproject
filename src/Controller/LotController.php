<?php


namespace App\Controller;


use App\Entity\Lot;
use App\Entity\RoundAuction;
use App\Entity\Car;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LotController extends BaseController
{

    protected function getModel() {
        $repo = $this->getRepo(Lot::class);
        return $repo;
    }

    /**
     * @Route("/lot-auction/create", name="create_lot_auction", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {

        $data = $this->transformJsonData($request);
        $message = (empty($data['car_vins'])) ? 'Нет данных (Vins)': '';
        $message = (empty($data['round_id'])) ? 'Не задан раунд аукциона' : '';
        if($message) {
            return $this->response(0, false, $message);
        }

        $carRepo = $this->getRepo(Car::class);
        $repository = $this->getModel();

        $profileId = (!empty($data['profile_id'])) ? $data['profile_id'] : 0;
        $lotStatus = (!empty($data['lot_status'])) ? $data['lot_status'] : 1;
        $auctionType = $data['auction_type'];
        $roundId     = $data['round_id'];
        $list = explode("\n", $data['car_vins']);
        $listCount = count($list);

        $ch = 0;
        foreach ($list as $vin) {
            $vin = trim($vin);
            if(!$vin) continue;
            $car = $carRepo->findOneBy(['vin' => $vin]);
            $car->getId();
            if(!empty($car)) {
                $carId = $car->getId();
                if($carId) {
                    $lot = [
                        'auction_type' => $auctionType,
                        'round_id'     => $roundId,
                        'car_id'       => $carId,
                        'profile_id'   => $profileId,
                        'lot_status'   => $lotStatus,
                    ];
                    $save = $repository->add($lot);
                    $ch++;
                }
            }
        }

//        $item = $this->getItem([
//            'car_id'   => $data['car_id'],
//            'round_id' => $data['round_id'],
//            'auction_type' => $data['auction_type'],
//        ]);
//        if(!empty($item)) {
//            $message = 'Такой лот уже существует';
//            return $this->response(0, false, $message);
//        }
//        $repository = $this->getModel();
//        $result     = $repository->add($data);
//        $status = ($result) ? true : false;
//        $item = $this->getItem(['id' => $result]);

        $message = "Общее количество: {$listCount}, создано {$ch} лотов";
        return $this->response($save, true,'Новые лоты созданы!' . $message);
    }

}