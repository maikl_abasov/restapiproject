<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Lot;
use App\Entity\RoundAuction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuctionController
 * @package App\Controller
 *
 * @Route(path="/auction")
 */


class AuctionController extends BaseController
{


    /**
     * @Route("/page/init/{auction_type}/{limit}", name="auction_page_init")
     */
    public function auctionPageInit($auction_type, $limit = 12): JsonResponse
    {
        // ---  /auction/page/init/1/10
        $pageDataBundle = $firstAuction = [];
        $firstAuctionCount = 0;

        // Получаем активные раунды аукционов
        $repo   = $this->getRepo(RoundAuction::class);
        $activeRounds = $repo->getActiveRounds($auction_type);

        // Получаем данные для формы поиска
        $repo   = $this->getRepo(Car::class);
        $searchFormData = $repo->getMarksAndModels($auction_type, true);

        // Получаем ближайщий по дате аукцион
         if(!empty($activeRounds[0]['id'])) {
             $firstAuctionCount = $activeRounds[0]['lot_count'];
             $roundId = $activeRounds[0]['id'];

             $repo   = $this->getRepo(Lot::class);
             $firstAuction = $repo->getActiveAuction($roundId, ['limit' => $limit]);
         }

        $pageDataBundle['active_rounds']    = $activeRounds;
        $pageDataBundle['search_form_init'] = $searchFormData;
        $pageDataBundle['first_auction']    = $firstAuction;
        $pageDataBundle['first_auction_count'] = $firstAuctionCount;

        // dd($pageDataBundle);
        return $this->response($pageDataBundle);
    }

    /**
     * @Route("/active-rounds/list/{auction_type}", name="auction_active_rounds")
     */
    public function getActiveRounds($auction_type): JsonResponse
    {
        $repo = $this->getRepo(RoundAuction::class);
        $result = $repo->getActiveRounds($auction_type, false);
        return $this->response($result);
    }

    /**
     * @Route("/search-form/marks/{auction_type}", name="auction_search_form_marks")
     */
    public function getFormMarks($auction_type): JsonResponse
    {
        $repo = $this->getRepo(Car::class);
        $result = $repo->getMarksAndModels($auction_type, true);
        return $this->response($result);
    }

    /**
     * @Route("/active-auction/list/{round_id}", name="auction_active_auction_list")
     */
    public function getActiveAuction($round_id, Request $request): JsonResponse
    {
        $formData = $this->transformJsonData($request);
        $repo   = $this->getRepo(Lot::class);
        $result = $repo->getActiveAuction($round_id, $formData);
        return $this->response($result);
    }

    /**
     * @Route("/active-auction/count/{round_id}", name="auction_active_auction_count")
     */
    public function getActiveAuctionCout($round_id): JsonResponse
    {
        $repo = $this->getRepo(Lot::class);
        $result = $repo->getActiveAuctionCount($round_id, false);
        return $this->response($result);
    }

}