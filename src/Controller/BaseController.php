<?php

namespace App\Controller;

use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\MyCustomTests\MyCustomTestService;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
//    protected $jwtStatus = false;
//    protected $userId    = 0;
//    protected $userRole  = 0;
//    protected $request;

    public function __construct()
    {
        // $this->request = Request::createFromGlobals();
    }

    protected function transformJsonData(Request $request, bool $convert = false)
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) return $request;
        $request->request->replace($data);
        $data = ($convert) ? (array)$data : $data;
        return $data;
    }

    protected function isParam($arr, $fieldName)
    {
        $result = (!empty($arr[$fieldName])) ? $arr[$fieldName] : '';
        return $result;
    }

    public function getRepo($class)
    {
        return $this->getDoctrine()->getRepository($class);
    }

    public function getDbConnect()
    {
        return $this->getDoctrine()->getConnection();
    }

    protected function response($result, $status = true, $message = '', $error = null) {
        return $this->json([
            'result'  => $result,
            'status'  => $status,
            'message' => $message,
            'error'   => $error,
        ]);
    }

    public function deleteItem(array $args)
    {
        $repository = $this->getModel();
        try {
            $result = $repository->remove($args);
        } catch (\Exception $exception) {
            $message = 'Ошибка в при удалении (' . $exception->getMessage() . ')';
            return $this->response(0, false, $message , $exception);
        }
        return $result;
    }

    // Получить одну запись из базы
    public function getItem(array $args, $convert = true)
    {
        $repository = $this->getModel();
        $item = $repository->findOneBy($args);
        if(empty($item))
            return false;
        if($convert) $item = $repository->convertToArray([$item], true);
        return $item;
    }

    // Получить лист записей из базы
    public function getItems(array $args = [], $convert = true)
    {
        $repository = $this->getModel();
        if(!empty($args)) {
            $list = $repository->findBy($args);
        } else {
            $list = $repository->findAll();
        }

        if(empty($list))
            return false;

        if($convert)
            $list = $repository->convertToArray($list);

        return $list;
    }

    protected function postCurl($url, $data, $method = 'POST') {

        $payload = json_encode($data);
        $headers =  [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload)
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        curl_close($ch);
        $info = curl_getinfo($ch);

        return [
            'result' => $result,
            'info' => $info,
        ];
    }

    protected function getSecretKey()
    {
        return 'gfh%6&*dgYU546mbnhBNtyyYU647BBsad';
    }

    protected function jwtTokenVerify()
    {
        $token = $this->request->headers->get('user-jwt-token');
        if (!$token) return false;
        $secretKey = $this->getSecretKey();
        try {
            $data = JWT::decode($token, $secretKey, ['HS256']);
            if (!empty($data->user_id)) {
                $this->userId   = $data->user_id;
                $this->userRole = $data->role;
                $this->jwtStatus = true;
            }

        } catch (\Exception $e) {
            dump($e->getMessage());
        }

        return $this->jwtStatus;
    }

    /**
     * @Route("/my-custom-tests-start", name="custom_tests")
     */

    public function myCustomTestsStart(): void
    {
        $repository = $this->getDbConnect();
        $testService = new MyCustomTestService($repository);
        $testService->start();
        exit;
    }

    /////////////////////////////
    ///
    ///
    ///

    public function testFunc() {

        $scheme  = $_SERVER['REQUEST_SCHEME'] . '://';
        $host    = $_SERVER['SERVER_NAME'];
        $baseUrl = $_SERVER['BASE'];

        $url = $scheme . $host . $baseUrl . '/user/create';

        $newUser = [
            "username" => "TesUserName-5",
            "password" => "123456",
            "email"    => "test123rtyygfhdhdhhd@mail.ru",
            "phone"    => "89056789087"
        ];

        $response = $this->postCurl($url, $newUser);
        print_r($response); die;
    }



//    public function findItem($class, $id, $format = 'array')
//    {
//        $repo = $this->getDoctrine()->getRepository($class);
//        $record = $repo->find($id);
//        if (empty($record)) return [];
//        $result = $this->formattedData($repo, [$record], $format);
//        return $result[0];
//    }
//
//    protected function findByItems($class, $conditions = [], $format = 'array')
//    {
//        $repo = $this->getDoctrine()->getRepository($class);
//        if (!empty($conditions)) {
//            $records = $repo->findBy($conditions);
//        } else {
//            $records = $repo->findAll();
//        }
//        if (empty($records)) return [];
//        $results = $this->formattedData($repo, $records, $format);
//        return $results;
//    }
//
//    public function formattedData($repo, $data, $format = 'array')
//    {
//        switch ($format) {
//            case 'array' :
//                $results = $repo->formatToArray($data);
//                break;
//            default :
//                $results = $data;
//                break;
//        }
//        return $results;
//    }
//
//    public function getRawData()
//    {
//        $data = file_get_contents("php://input");
//        $data = json_decode($data);
//        return $data;
//    }

}