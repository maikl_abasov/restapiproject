<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $run;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $pts;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mark_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $model_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $modification_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $generation_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $start_price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max_price;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nds;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $place;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $profile_id;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $video;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getRun(): ?string
    {
        return $this->run;
    }

    public function setRun(?string $run): self
    {
        $this->run = $run;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function setCreatedAt(?string $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?string $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getPts(): ?string
    {
        return $this->pts;
    }

    public function setPts(?string $pts): self
    {
        $this->pts = $pts;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getMarkId(): ?int
    {
        return $this->mark_id;
    }

    public function setMarkId(?int $mark_id): self
    {
        $this->mark_id = $mark_id;

        return $this;
    }

    public function getModelId(): ?int
    {
        return $this->model_id;
    }

    public function setModelId(?int $model_id): self
    {
        $this->model_id = $model_id;

        return $this;
    }

    public function getModificationId(): ?int
    {
        return $this->modification_id;
    }

    public function setModificationId(?int $modification_id): self
    {
        $this->modification_id = $modification_id;

        return $this;
    }

    public function getGenerationId(): ?int
    {
        return $this->generation_id;
    }

    public function setGenerationId(?int $generation_id): self
    {
        $this->generation_id = $generation_id;

        return $this;
    }

    public function getStartPrice(): ?int
    {
        return $this->start_price;
    }

    public function setStartPrice(?int $start_price): self
    {
        $this->start_price = $start_price;

        return $this;
    }

    public function getMaxPrice(): ?int
    {
        return $this->max_price;
    }

    public function setMaxPrice(?int $max_price): self
    {
        $this->max_price = $max_price;

        return $this;
    }

    public function getNds(): ?string
    {
        return $this->nds;
    }

    public function setNds(?string $nds): self
    {
        $this->nds = $nds;

        return $this;
    }

    public function getCity(): ?int
    {
        return $this->city;
    }

    public function setCity(?int $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getPropsToArray(): array
    {
        return (array)get_object_vars($this);
    }

    public function getProfileId(): ?int
    {
        return $this->profile_id;
    }

    public function setProfileId(?int $profile_id): self
    {
        $this->profile_id = $profile_id;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

}
