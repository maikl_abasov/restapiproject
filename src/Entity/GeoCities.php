<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeoCities
 *
 * @ORM\Table(name="geo_cities", uniqueConstraints={@ORM\UniqueConstraint(name="geo__cities_sort_region_uindex", columns={"sort_region"}), @ORM\UniqueConstraint(name="geo__cities_sort_uindex", columns={"sort_city"})})
 * @ORM\Entity
 */
class GeoCities
{
    /**
     * @var int
     *
     * @ORM\Column(name="city_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cityId;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=128, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=128, nullable=false)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="district", type="string", length=128, nullable=false)
     */
    private $district;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=0, nullable=false)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float", precision=10, scale=0, nullable=false)
     */
    private $lng;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_city", type="integer", nullable=true)
     */
    private $sortCity;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_region", type="integer", nullable=true)
     */
    private $sortRegion;

    /**
     * @var int
     *
     * @ORM\Column(name="delivery", type="integer", nullable=false)
     */
    private $delivery = '0';

    public function getCityId(): ?int
    {
        return $this->cityId;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(string $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getSortCity(): ?int
    {
        return $this->sortCity;
    }

    public function setSortCity(?int $sortCity): self
    {
        $this->sortCity = $sortCity;

        return $this;
    }

    public function getSortRegion(): ?int
    {
        return $this->sortRegion;
    }

    public function setSortRegion(?int $sortRegion): self
    {
        $this->sortRegion = $sortRegion;

        return $this;
    }

    public function getDelivery(): ?int
    {
        return $this->delivery;
    }

    public function setDelivery(int $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }


}
