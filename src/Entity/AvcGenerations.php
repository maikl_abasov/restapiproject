<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvcGenerations
 *
 * @ORM\Table(name="avc_generations", indexes={@ORM\Index(name="model", columns={"model"})})
 * @ORM\Entity
 */
class AvcGenerations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="model", type="integer", nullable=true)
     */
    private $model;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=256, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_car_generation", type="string", length=100, nullable=true)
     */
    private $idCarGeneration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?int
    {
        return $this->model;
    }

    public function setModel(?int $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIdCarGeneration(): ?string
    {
        return $this->idCarGeneration;
    }

    public function setIdCarGeneration(?string $idCarGeneration): self
    {
        $this->idCarGeneration = $idCarGeneration;

        return $this;
    }


}
