<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvcModifications
 *
 * @ORM\Table(name="avc_modifications", indexes={@ORM\Index(name="generation", columns={"generation"})})
 * @ORM\Entity
 */
class AvcModifications
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="generation", type="integer", nullable=true)
     */
    private $generation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=256, nullable=true)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="year_from", type="integer", nullable=true)
     */
    private $yearFrom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="year_to", type="integer", nullable=true)
     */
    private $yearTo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="body_type", type="string", length=128, nullable=true)
     */
    private $bodyType;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="doors", type="boolean", nullable=true)
     */
    private $doors;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fuel_type", type="string", length=128, nullable=true)
     */
    private $fuelType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="drive_type", type="string", length=128, nullable=true)
     */
    private $driveType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="transmission", type="string", length=128, nullable=true)
     */
    private $transmission;

    /**
     * @var int|null
     *
     * @ORM\Column(name="power", type="integer", nullable=true)
     */
    private $power;

    /**
     * @var float|null
     *
     * @ORM\Column(name="engine_size", type="float", precision=10, scale=0, nullable=true)
     */
    private $engineSize;

    /**
     * @var string|null
     *
     * @ORM\Column(name="options_id", type="string", length=100, nullable=true)
     */
    private $optionsId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_car_modification", type="string", length=100, nullable=true)
     */
    private $idCarModification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGeneration(): ?int
    {
        return $this->generation;
    }

    public function setGeneration(?int $generation): self
    {
        $this->generation = $generation;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYearFrom(): ?int
    {
        return $this->yearFrom;
    }

    public function setYearFrom(?int $yearFrom): self
    {
        $this->yearFrom = $yearFrom;

        return $this;
    }

    public function getYearTo(): ?int
    {
        return $this->yearTo;
    }

    public function setYearTo(?int $yearTo): self
    {
        $this->yearTo = $yearTo;

        return $this;
    }

    public function getBodyType(): ?string
    {
        return $this->bodyType;
    }

    public function setBodyType(?string $bodyType): self
    {
        $this->bodyType = $bodyType;

        return $this;
    }

    public function getDoors(): ?bool
    {
        return $this->doors;
    }

    public function setDoors(?bool $doors): self
    {
        $this->doors = $doors;

        return $this;
    }

    public function getFuelType(): ?string
    {
        return $this->fuelType;
    }

    public function setFuelType(?string $fuelType): self
    {
        $this->fuelType = $fuelType;

        return $this;
    }

    public function getDriveType(): ?string
    {
        return $this->driveType;
    }

    public function setDriveType(?string $driveType): self
    {
        $this->driveType = $driveType;

        return $this;
    }

    public function getTransmission(): ?string
    {
        return $this->transmission;
    }

    public function setTransmission(?string $transmission): self
    {
        $this->transmission = $transmission;

        return $this;
    }

    public function getPower(): ?int
    {
        return $this->power;
    }

    public function setPower(?int $power): self
    {
        $this->power = $power;

        return $this;
    }

    public function getEngineSize(): ?float
    {
        return $this->engineSize;
    }

    public function setEngineSize(?float $engineSize): self
    {
        $this->engineSize = $engineSize;

        return $this;
    }

    public function getOptionsId(): ?string
    {
        return $this->optionsId;
    }

    public function setOptionsId(?string $optionsId): self
    {
        $this->optionsId = $optionsId;

        return $this;
    }

    public function getIdCarModification(): ?string
    {
        return $this->idCarModification;
    }

    public function setIdCarModification(?string $idCarModification): self
    {
        $this->idCarModification = $idCarModification;

        return $this;
    }


}
