<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvcMarks
 *
 * @ORM\Table(name="avc_marks")
 * @ORM\Entity
 */
class AvcMarks
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name_rus", type="string", length=100, nullable=true)
     */
    private $nameRus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_car_mark", type="string", length=100, nullable=true)
     */
    private $idCarMark;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameRus(): ?string
    {
        return $this->nameRus;
    }

    public function setNameRus(?string $nameRus): self
    {
        $this->nameRus = $nameRus;

        return $this;
    }

    public function getIdCarMark(): ?string
    {
        return $this->idCarMark;
    }

    public function setIdCarMark(?string $idCarMark): self
    {
        $this->idCarMark = $idCarMark;

        return $this;
    }


}
