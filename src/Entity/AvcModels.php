<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvcModels
 *
 * @ORM\Table(name="avc_models", indexes={@ORM\Index(name="mark", columns={"mark"})})
 * @ORM\Entity
 */
class AvcModels
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark", type="integer", nullable=true)
     */
    private $mark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=256, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name_rus", type="string", length=100, nullable=true)
     */
    private $nameRus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_car_model", type="string", length=100, nullable=true)
     */
    private $idCarModel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMark(): ?int
    {
        return $this->mark;
    }

    public function setMark(?int $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameRus(): ?string
    {
        return $this->nameRus;
    }

    public function setNameRus(?string $nameRus): self
    {
        $this->nameRus = $nameRus;

        return $this;
    }

    public function getIdCarModel(): ?string
    {
        return $this->idCarModel;
    }

    public function setIdCarModel(?string $idCarModel): self
    {
        $this->idCarModel = $idCarModel;

        return $this;
    }


}
