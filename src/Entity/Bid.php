<?php

namespace App\Entity;

use App\Repository\BidRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BidRepository::class)
 */
class Bid
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lot_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $profile_id;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $round_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $confirm_state;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $confirm_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLotId(): ?int
    {
        return $this->lot_id;
    }

    public function setLotId(?int $lot_id): self
    {
        $this->lot_id = $lot_id;

        return $this;
    }

    public function getProfileId(): ?int
    {
        return $this->profile_id;
    }

    public function setProfileId(?int $profile_id): self
    {
        $this->profile_id = $profile_id;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function setCreatedAt(?string $created_id): self
    {
        $this->created_at = $created_id;

        return $this;
    }

    public function getRoundId(): ?int
    {
        return $this->round_id;
    }

    public function setRoundId(?int $round_id): self
    {
        $this->round_id = $round_id;

        return $this;
    }

    public function getConfirmState(): ?int
    {
        return $this->confirm_state;
    }

    public function setConfirmState(?int $confirm_state): self
    {
        $this->confirm_state = $confirm_state;

        return $this;
    }

    public function getConfirmDate(): ?string
    {
        return $this->confirm_date;
    }

    public function setConfirmDate(?string $confirm_date): self
    {
        $this->confirm_date = $confirm_date;

        return $this;
    }
}
