<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CbnLots
 *
 * @ORM\Table(name="cbn_lots", indexes={@ORM\Index(name="acc_id", columns={"acc_id"})})
 * @ORM\Entity
 */
class CbnLots
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="acc_id", type="integer", nullable=true)
     */
    private $accId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="num", type="integer", nullable=true, options={"default"="100"})
     */
    private $num = 100;

    /**
     * @var int|null
     *
     * @ORM\Column(name="category", type="integer", nullable=true)
     */
    private $category;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="city", type="integer", nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(name="l_type", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $lType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ascdesc", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $ascdesc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="start_price", type="integer", nullable=true)
     */
    private $startPrice;

    /**
     * @var int|null
     *
     * @ORM\Column(name="price_step", type="integer", nullable=true)
     */
    private $priceStep;

    /**
     * @var int|null
     *
     * @ORM\Column(name="price_now", type="integer", nullable=true)
     */
    private $priceNow;

    /**
     * @var int|null
     *
     * @ORM\Column(name="price_min", type="integer", nullable=true)
     */
    private $priceMin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nds", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $nds;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark", type="integer", nullable=true)
     */
    private $mark;

    /**
     * @var int|null
     *
     * @ORM\Column(name="model", type="integer", nullable=true)
     */
    private $model;

    /**
     * @var int|null
     *
     * @ORM\Column(name="generation", type="integer", nullable=true)
     */
    private $generation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="modification", type="integer", nullable=true)
     */
    private $modification;

    /**
     * @var string|null
     *
     * @ORM\Column(name="self_modification", type="text", length=65535, nullable=true)
     */
    private $selfModification;

    /**
     * @var string|null
     *
     * @ORM\Column(name="body", type="string", length=30, nullable=true)
     */
    private $body;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vin", type="string", length=18, nullable=true)
     */
    private $vin;

    /**
     * @var int|null
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mileage", type="integer", nullable=true)
     */
    private $mileage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="engine", type="string", length=20, nullable=true)
     */
    private $engine;

    /**
     * @var string|null
     *
     * @ORM\Column(name="engine_vol", type="string", length=50, nullable=true)
     */
    private $engineVol;

    /**
     * @var int|null
     *
     * @ORM\Column(name="power", type="integer", nullable=true)
     */
    private $power;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gearbox", type="string", length=30, nullable=true)
     */
    private $gearbox;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=50, nullable=true)
     */
    private $color;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="key_qty", type="boolean", nullable=true)
     */
    private $keyQty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reg_plate", type="string", length=50, nullable=true)
     */
    private $regPlate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="drive", type="string", length=20, nullable=true)
     */
    private $drive;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pts", type="string", length=20, nullable=true)
     */
    private $pts;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pts_no", type="string", length=20, nullable=true)
     */
    private $ptsNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sts_no", type="string", length=20, nullable=true)
     */
    private $stsNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serv_book", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $servBook;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comments", type="text", length=65535, nullable=true)
     */
    private $comments;

    /**
     * @var string|null
     *
     * @ORM\Column(name="view_place", type="text", length=65535, nullable=true)
     */
    private $viewPlace;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dkp", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $dkp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $status = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="admin_comment", type="text", length=65535, nullable=true)
     */
    private $adminComment;

    /**
     * @var string|null
     *
     * @ORM\Column(name="view_descr", type="text", length=65535, nullable=true)
     */
    private $viewDescr;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pay_block", type="integer", nullable=true)
     */
    private $payBlock = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="comission", type="integer", nullable=true)
     */
    private $comission = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="owners_count", type="string", length=150, nullable=true)
     */
    private $ownersCount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="auto_complectation", type="string", length=200, nullable=true)
     */
    private $autoComplectation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="round_id", type="integer", nullable=true)
     */
    private $roundId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="classified_status", type="string", length=50, nullable=true)
     */
    private $classifiedStatus;

    /**
     * @var int|null
     *
     * @ORM\Column(name="freedelivery_status", type="integer", nullable=true)
     */
    private $freedeliveryStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="car_class_code", type="string", length=1, nullable=false)
     */
    private $carClassCode = '';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_dt", type="datetime", nullable=true)
     */
    private $createdDt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fid_load_type", type="string", length=100, nullable=true)
     */
    private $fidLoadType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccId(): ?int
    {
        return $this->accId;
    }

    public function setAccId(?int $accId): self
    {
        $this->accId = $accId;

        return $this;
    }

    public function getNum(): ?int
    {
        return $this->num;
    }

    public function setNum(?int $num): self
    {
        $this->num = $num;

        return $this;
    }

    public function getCategory(): ?int
    {
        return $this->category;
    }

    public function setCategory(?int $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(?\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getCity(): ?int
    {
        return $this->city;
    }

    public function setCity(?int $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLType(): ?string
    {
        return $this->lType;
    }

    public function setLType(?string $lType): self
    {
        $this->lType = $lType;

        return $this;
    }

    public function getAscdesc(): ?string
    {
        return $this->ascdesc;
    }

    public function setAscdesc(?string $ascdesc): self
    {
        $this->ascdesc = $ascdesc;

        return $this;
    }

    public function getStartPrice(): ?int
    {
        return $this->startPrice;
    }

    public function setStartPrice(?int $startPrice): self
    {
        $this->startPrice = $startPrice;

        return $this;
    }

    public function getPriceStep(): ?int
    {
        return $this->priceStep;
    }

    public function setPriceStep(?int $priceStep): self
    {
        $this->priceStep = $priceStep;

        return $this;
    }

    public function getPriceNow(): ?int
    {
        return $this->priceNow;
    }

    public function setPriceNow(?int $priceNow): self
    {
        $this->priceNow = $priceNow;

        return $this;
    }

    public function getPriceMin(): ?int
    {
        return $this->priceMin;
    }

    public function setPriceMin(?int $priceMin): self
    {
        $this->priceMin = $priceMin;

        return $this;
    }

    public function getNds(): ?string
    {
        return $this->nds;
    }

    public function setNds(?string $nds): self
    {
        $this->nds = $nds;

        return $this;
    }

    public function getMark(): ?int
    {
        return $this->mark;
    }

    public function setMark(?int $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getModel(): ?int
    {
        return $this->model;
    }

    public function setModel(?int $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getGeneration(): ?int
    {
        return $this->generation;
    }

    public function setGeneration(?int $generation): self
    {
        $this->generation = $generation;

        return $this;
    }

    public function getModification(): ?int
    {
        return $this->modification;
    }

    public function setModification(?int $modification): self
    {
        $this->modification = $modification;

        return $this;
    }

    public function getSelfModification(): ?string
    {
        return $this->selfModification;
    }

    public function setSelfModification(?string $selfModification): self
    {
        $this->selfModification = $selfModification;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getMileage(): ?int
    {
        return $this->mileage;
    }

    public function setMileage(?int $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getEngine(): ?string
    {
        return $this->engine;
    }

    public function setEngine(?string $engine): self
    {
        $this->engine = $engine;

        return $this;
    }

    public function getEngineVol(): ?string
    {
        return $this->engineVol;
    }

    public function setEngineVol(?string $engineVol): self
    {
        $this->engineVol = $engineVol;

        return $this;
    }

    public function getPower(): ?int
    {
        return $this->power;
    }

    public function setPower(?int $power): self
    {
        $this->power = $power;

        return $this;
    }

    public function getGearbox(): ?string
    {
        return $this->gearbox;
    }

    public function setGearbox(?string $gearbox): self
    {
        $this->gearbox = $gearbox;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getKeyQty(): ?bool
    {
        return $this->keyQty;
    }

    public function setKeyQty(?bool $keyQty): self
    {
        $this->keyQty = $keyQty;

        return $this;
    }

    public function getRegPlate(): ?string
    {
        return $this->regPlate;
    }

    public function setRegPlate(?string $regPlate): self
    {
        $this->regPlate = $regPlate;

        return $this;
    }

    public function getDrive(): ?string
    {
        return $this->drive;
    }

    public function setDrive(?string $drive): self
    {
        $this->drive = $drive;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPts(): ?string
    {
        return $this->pts;
    }

    public function setPts(?string $pts): self
    {
        $this->pts = $pts;

        return $this;
    }

    public function getPtsNo(): ?string
    {
        return $this->ptsNo;
    }

    public function setPtsNo(?string $ptsNo): self
    {
        $this->ptsNo = $ptsNo;

        return $this;
    }

    public function getStsNo(): ?string
    {
        return $this->stsNo;
    }

    public function setStsNo(?string $stsNo): self
    {
        $this->stsNo = $stsNo;

        return $this;
    }

    public function getServBook(): ?string
    {
        return $this->servBook;
    }

    public function setServBook(?string $servBook): self
    {
        $this->servBook = $servBook;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getViewPlace(): ?string
    {
        return $this->viewPlace;
    }

    public function setViewPlace(?string $viewPlace): self
    {
        $this->viewPlace = $viewPlace;

        return $this;
    }

    public function getDkp(): ?string
    {
        return $this->dkp;
    }

    public function setDkp(?string $dkp): self
    {
        $this->dkp = $dkp;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAdminComment(): ?string
    {
        return $this->adminComment;
    }

    public function setAdminComment(?string $adminComment): self
    {
        $this->adminComment = $adminComment;

        return $this;
    }

    public function getViewDescr(): ?string
    {
        return $this->viewDescr;
    }

    public function setViewDescr(?string $viewDescr): self
    {
        $this->viewDescr = $viewDescr;

        return $this;
    }

    public function getPayBlock(): ?int
    {
        return $this->payBlock;
    }

    public function setPayBlock(?int $payBlock): self
    {
        $this->payBlock = $payBlock;

        return $this;
    }

    public function getComission(): ?int
    {
        return $this->comission;
    }

    public function setComission(?int $comission): self
    {
        $this->comission = $comission;

        return $this;
    }

    public function getOwnersCount(): ?string
    {
        return $this->ownersCount;
    }

    public function setOwnersCount(?string $ownersCount): self
    {
        $this->ownersCount = $ownersCount;

        return $this;
    }

    public function getAutoComplectation(): ?string
    {
        return $this->autoComplectation;
    }

    public function setAutoComplectation(?string $autoComplectation): self
    {
        $this->autoComplectation = $autoComplectation;

        return $this;
    }

    public function getRoundId(): ?int
    {
        return $this->roundId;
    }

    public function setRoundId(?int $roundId): self
    {
        $this->roundId = $roundId;

        return $this;
    }

    public function getClassifiedStatus(): ?string
    {
        return $this->classifiedStatus;
    }

    public function setClassifiedStatus(?string $classifiedStatus): self
    {
        $this->classifiedStatus = $classifiedStatus;

        return $this;
    }

    public function getFreedeliveryStatus(): ?int
    {
        return $this->freedeliveryStatus;
    }

    public function setFreedeliveryStatus(?int $freedeliveryStatus): self
    {
        $this->freedeliveryStatus = $freedeliveryStatus;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getCarClassCode(): ?string
    {
        return $this->carClassCode;
    }

    public function setCarClassCode(string $carClassCode): self
    {
        $this->carClassCode = $carClassCode;

        return $this;
    }

    public function getCreatedDt(): ?\DateTimeInterface
    {
        return $this->createdDt;
    }

    public function setCreatedDt(?\DateTimeInterface $createdDt): self
    {
        $this->createdDt = $createdDt;

        return $this;
    }

    public function getFidLoadType(): ?string
    {
        return $this->fidLoadType;
    }

    public function setFidLoadType(?string $fidLoadType): self
    {
        $this->fidLoadType = $fidLoadType;

        return $this;
    }


}
