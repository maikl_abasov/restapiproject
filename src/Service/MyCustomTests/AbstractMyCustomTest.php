<?php


namespace App\Service\MyCustomTests;


abstract class AbstractMyCustomTest
{

    protected function send($url, $params = [], $method = 'GET')
    {
        $response = $this->client->request($method, $url, $params);
        return $response;
    }

    protected function setMessage($message) {
        $this->testMessages[] = $message;
    }

    protected function getServerUrl() {
        $scheme  = $_SERVER['REQUEST_SCHEME'] . '://';
        $host    = $_SERVER['SERVER_ADDR'];
        $port    = $_SERVER['SERVER_PORT'];
        $url = $scheme . $host .':'. $port ;
        // print_r($_SERVER); die;
        return $url;
    }

    protected function postCurl($url, $data, $method = 'POST')
    {
        $this->setMessage('Url:' . $url);

        $payload = json_encode($data);
        $headers = [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload)
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $result = $this->curlCheckedResponse($response, $info, $data);
        return [
            'result' => (array)$result,
            'info'   => $info,
        ];
    }

    protected function getCurl($url, $method = 'GET')
    {
        $headers = ['Content-Type: application/json'];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        $info    = curl_getinfo($ch);
        curl_close($ch);
        $result = $this->curlCheckedResponse($response, $info);
        return [
            'result' => (array)$result,
            'info'   => $info,
        ];
    }

    protected function curlCheckedResponse($response, $info, $postData = []) {
        $result = json_decode($response);
        $error = json_last_error_msg();

        if(is_string($result) || $error != 'No error') {
            echo '<div style="color:red;">===== IN CURL FATAL ERROR ====</div>';

            echo '<div style="color:red;">Response</div>';
            print_r($response);

            echo '<div style="color:red;">postData</div>';
            $out = print_r($postData, true);
            echo "<pre>{$out}</pre>";

            echo '<div style="color:red;">CurlInfo</div>';
            $out = print_r($info, true);
            echo "<pre>{$out}</pre>";

            exit;
        }

        return $result;
    }

    protected function lg($data) {
        $out = print_r($data, true);
        die("<pre>{$out}</pre>");
    }

}