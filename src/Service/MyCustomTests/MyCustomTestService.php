<?php

namespace App\Service\MyCustomTests;

use GuzzleHttp\Client;

class MyCustomTestService extends AbstractMyCustomTest
{
    protected $client;
    protected $apiUrl;
    protected $testMessages;
    protected $repo;

    public function __construct($repo = null)
    {
        $this->repo = $repo;
        $this->client = new Client(['base_uri' => '/']);
        $this->apiUrl = $this->getServerUrl();
    }

    // Запуск всех сценариев
    public function start()
    {
        // $this->testProfile();
        // $this->testCar();
        // $this->testRoundAuction();
        // $this->testLot();
        // $this->testBid();

        // $this->testCarFixture();

        die('test-ok');
    }

    protected function renderResponse($response, $url) {
        $this->setMessage('<br><br>');
        $this->setMessage('--------------Start-----------------');
        $this->setMessage('Url:' . $url);
        $curlInfo = $response['info'];

        if(!empty($response['result'])) {
            $res = (array)json_decode($response['result']);
            $this->setMessage('Запрос выполнен');

            $code = $curlInfo['http_code'];
            $color = ($code == 200) ? 'green' : 'red';
            $codeBlock = "<span style='color: {$color}; margin-left:0px' >Код ответа: {$code}</span>";
            $this->setMessage($codeBlock);

            $result  = (isset($res['result'])) ? $res['result'] : '';
            $message = (isset($res['message'])) ? $res['message'] : '';
            $status  = (isset($res['status'])) ? $res['status'] : '';
            $error   = (isset($res['error'])) ? $res['error'] : '';

            $this->setMessage('Статус:' . $status);

            if(!empty($res['status'])) {
                $this->setMessage('Успешно:' . $message);
            } else {
                $this->setMessage('Неудачно:' . $message);
            }

        } else {
            $this->setMessage('Запрос не выполнился, фатальная ошибка');
        }

        $this->setMessage('--------------End-----------------');
        $this->setMessage('<br><br>');

        return $result;
    }

    protected function testProfile()
    {

        $API_URL = $this->apiUrl . '/profile';

        // --------------------------
        // тестируем создание профиля
        $num = 4;
        $newProfile = [
            "name"    => "TesProfileName_" . $num,
            "email"   => "test{$num}@mail.ru",
            "user_id" => 2,
            "type"    => 1,
            "phone"   => "89056789087",
        ];

        $URL = $API_URL . '/create';
        $this->setMessage('Url:' . $URL);
        $response = $this->postCurl($URL, $newProfile, 'POST');
        $curlInfo = $response['info'];

        if(!empty($response['result'])) {
            $res = (array)json_decode($response['result']);

            $this->setMessage('Запрос выполнен');
            $this->setMessage('Код ответа:' . $curlInfo['http_code']);

            $result = $profileId = $res['result'];
            $message = $res['message'];
            $status  = $res['status'];
            $error   = $res['error'];

            // $this->lg($res);

            if(!empty($res['status'])) {

                $this->setMessage('Успешное создание:' . $message);

                // тестируем выборку списка
                $URL = $API_URL . '/list/all';
                $response = $this->getCurl($URL);
                $res = $this->renderResponse($response, $URL);

                // тестируем выборку одной записи
                $URL = $API_URL . '/item/' . $profileId . '/id';
                $response = $this->getCurl($URL);
                $res = $this->renderResponse($response, $URL);
                $res = (array)$res;
                if(!empty($res['name'])) {
                    $email = $res['email'];
                    $this->setMessage('Имя профиля:' . $res['name']);
                }

                // тестируем удаление
                $URL = $API_URL . '/delete/' . $profileId;
                $response = $this->getCurl($URL);
                $res = $this->renderResponse($response, $URL);

                // тестируем на упешное удаление
                $URL = $API_URL . '/item/' . $profileId . '/id';
                $response = $this->getCurl($URL);
                $res = $this->renderResponse($response, $URL);
                $res = (array)$res;
                if(isset($res['name'])) {
                    $email = $res['email'];
                    $this->setMessage('Имя профиля:' . $res['name']);
                }

            } else {
                $this->setMessage('Ошибка в контроллере:' . $message);
            }

        } else {
            $this->setMessage('Запрос не выполнился, фатальная ошибка');
        }

        $this->lg([
            'test' => 'Profile',
            'messages' => $this->testMessages,
            'response' => $response,
        ]);

    }

    protected function testCar()
    {
        $API_URL = $this->apiUrl . '/car';

        // тестируем создание авто
        $newItem = [
            'mark_id'         => 2,
            'model_id'        => 5,
            'modification_id' => 23,
            'generation_id'   => 12,
            'vin'             => 'vin_' . rand(5, 15000),
            'run'             => '12500',
            'year'            => '2013',
            'pts'             => 'pts-23456',
            'nds'             => 1,
            'city'            => 23,
            'comment'         => 'Новые комментарии',
            'start_price'     => 1500350,
            'max_price'       => 2200300,
            'place'           => 'Ул. Правды, дом 32',
        ];

        $URL = $API_URL . '/create';
        $this->setMessage('Url:' . $URL);
        $response = $this->postCurl($URL, $newItem, 'POST');
        // $response = $this->send($URL, $newItem, 'POST');
        $this->lg($response);

        $curlInfo = $response['info'];

//        if(!empty($response['result'])) {
//            $res = (array)json_decode($response['result']);
//
//            $this->setMessage('Запрос выполнен');
//            $this->setMessage('Код ответа:' . $curlInfo['http_code']);
//
//            $result = $profileId = $res['result'];
//            $message = $res['message'];
//            $status  = $res['status'];
//            $error   = $res['error'];
//
//            // $this->lg($res);
//
//            if(!empty($res['status'])) {
//
//                $this->setMessage('Успешное создание:' . $message);
//
//                // тестируем выборку списка
//                $URL = $API_URL . '/list/all';
//                $response = $this->getCurl($URL);
//                $res = $this->renderResponse($response, $URL);
//
//                // тестируем выборку одной записи
//                $URL = $API_URL . '/item/' . $profileId . '/id';
//                $response = $this->getCurl($URL);
//                $res = $this->renderResponse($response, $URL);
//                $res = (array)$res;
//                if(!empty($res['name'])) {
//                    $email = $res['email'];
//                    $this->setMessage('Имя профиля:' . $res['name']);
//                }
//
//                // тестируем удаление
//                $URL = $API_URL . '/delete/' . $profileId;
//                $response = $this->getCurl($URL);
//                $res = $this->renderResponse($response, $URL);
//
//                // тестируем на упешное удаление
//                $URL = $API_URL . '/item/' . $profileId . '/id';
//                $response = $this->getCurl($URL);
//                $res = $this->renderResponse($response, $URL);
//                $res = (array)$res;
//                if(isset($res['name'])) {
//                    $email = $res['email'];
//                    $this->setMessage('Имя профиля:' . $res['name']);
//                }
//
//            } else {
//                $this->setMessage('Ошибка в контроллере:' . $message);
//            }
//
//        } else {
//            $this->setMessage('Запрос не выполнился, фатальная ошибка');
//        }

        $this->lg([
            'test' => 'Car',
            'messages' => $this->testMessages,
            'response' => $response,
        ]);

    }

    protected function testRoundAuction()
    {
        $API_URL = $this->apiUrl . '/round-auction';

        // тестируем создание раунда
        $day = 20;
        $newItem = [
            'start_date'   => '2022-10-'.$day.' 10:00',
            'end_date'     => '2022-10-'.$day.' 15:00',
            'auction_type' => 1,
        ];

        $URL = $API_URL . '/create';
        $this->setMessage('Url:' . $URL);
        $response = $this->postCurl($URL, $newItem, 'POST');
        $curlInfo = $response['info'];

        $this->lg([
            'test'     => 'RoundAuction',
            'messages' => $this->testMessages,
            'response' => $response,
        ]);
    }

    protected function testLot()
    {
        $API_URL = $this->apiUrl . '/lot-auction';

        $newItem = [
            'car_id'       => 4,
            'round_id'     => 5,
            'auction_type' => 1,
        ];

        $URL = $API_URL . '/create';
        $response = $this->postCurl($URL, $newItem, 'POST');
        $status = $this->renderJsonResult($response);
        if($status) {
            $place = '<br>' . __LINE__ . '<br' . __FILE__;
            $this->lg(['Ошибка при создании:' . $place , $response]);
        }

//        $URL = $API_URL . '/list/all';
//        $response = $this->postCurl($URL, $newItem, 'POST');
//        $status = $this->renderJsonResult($response);
//        if(empty($result))

        $this->lg([
            'test'     => 'Lot',
            'result'   => $response['result'],
            'messages' => $this->testMessages,
            'response' => $response,
        ]);
    }

    protected function testBid()
    {
        $API_URL = $this->apiUrl . '/auction/bid';

        $newItem = [
            'lot_id'     => 4,
            'bid_price'  => 1500100,
            'profile_id' => 1,
        ];

        $URL = $API_URL . '/create';
        $response = $this->postCurl($URL, $newItem, 'POST');
        $status = $this->renderJsonResult($response);
        if($status) {
            $place = '<br>' . __LINE__ . '<br' . __FILE__;
            $this->lg(['Ошибка при создании:' . $place , $response]);
        }

//        $URL = $API_URL . '/list/all';
//        $response = $this->postCurl($URL, $newItem, 'POST');
//        $status = $this->renderJsonResult($response);
//        if(empty($result))

        $this->lg([
            'test'     => 'Lot',
            'result'   => $response['result'],
            'messages' => $this->testMessages,
            'response' => $response,
        ]);
    }

    protected function testCarFixture()
    {
        $API_URL = $this->apiUrl . '/car';

        $query = " SELECT * FROM `cbn_lots`";

        $conn = $this->repo;
        $stmt = $conn->prepare($query);
        $resultSet = $stmt->executeQuery([]);
        $lots    = $resultSet->fetchAllAssociative();

        // $this->lg([$API_URL]);

        $profileId = 17;

        foreach ($lots as $item) {


            $genId = ($item['generation'] > 290937) ? $item['generation'] - 290937 : $item['generation'];
            $modId = ($item['modification'] > 290937) ? $item['modification'] - 290937 : $item['modification'];

            if(!$genId || !$modId) continue;

            $newItem = [
                'mark_id'   => $item['mark'],
                'model_id'  => $item['model'],
                'modification_id' => $modId,
                'generation_id'   => $genId,
                'vin'  =>  $item['vin'] . rand(5, 1500),
                'run'  =>  rand(5555, 999999),
                'year' =>  $item['year'],
                'pts'  =>  $item['pts_no'],
                'nds'  =>  $item['nds'],
                'city' =>  $item['city'],

                'comment'     => $item['comments'],
                'start_price' => $item['start_price'],
                'max_price'   => $item['price_min'],
                'place'       => $item['view_place'],

                'color'       => $item['color'],
                'video'       => $item['video'],
                'profile_id'  => $profileId,
                //'register_num' => $item['reg_plate'],
            ];

            $URL = $API_URL . '/create';
            $this->setMessage('Url:' . $URL);
            $response = $this->postCurl($URL, $newItem, 'POST');

        }

        die('Ok');

//            [acc_id] => 6
//            [city]   => 889
//            [start_price] => 380000
//            [price_step] => 10000
//            [price_now] => 435000
//            [price_min] => 405000
//            [nds] => 1
//            [mark] => 119
//            [model] => 650
//            [generation] => 333430
//            [modification] => 472500
//            [vin] => JMBSTCY3A8U009066
//            [year] => 2008
//            [color] => синий
//            [reg_plate] => О586ОВ31
//            [description] =>
//            [pts] => Оригинал ПТС
//            [pts_no] => 78 УА086390
//            [sts_no] => 904643721
//
//            [comments] => Описание автомобиля:
//                Техническое состояние:
//                Течь прокладки клапанной крышки.
//                Течь прокладки поддона.
//                Пинок при включении передачи.
//                Задние стойки стабилизаторов требует замены.
//                Передние диски требуют замены.
//                Стояночный тормоз не работает.
//                Не работает наружная ручка правой двери.
//                Кондиционер не работает.
//
//            [view_place] => Воронеж, Остужева 47/7
//            [admin_comment] =>
//            [view_descr] =>
//            [pay_block] => 0
//            [comission] => 0
//            [video] =>
//            [car_class_code] => C

    }

    protected function renderJsonResult($response) {
        $curlInfo = $response['info'];
        $result   = $response['result'];
        $color = 'green';

        if(empty($result['status'])) {
            $color = 'red';
        }

        $status = $result['status'];
        $message = $result['message'];
        $message = "<span style='{$color}'>{$message}</span>";
        $this->setMessage($message);
        return $status;
    }

}