<?php

namespace App\Repository;

use App\Entity\Bid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Bid>
 *
 * @method Bid|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bid|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bid[]    findAll()
 * @method Bid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BidRepository extends ServiceEntityRepository
{
    use TraitUtilsRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bid::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setBid(array $data , bool $flush = true)
    {
        $today = $this->getDateTime();

        $model = new Bid();
        $model->setLotId($data["lot_id"]);
        $model->setProfileId($data["profile_id"]);
        $model->setPrice($data["bid_price"]);
        if(!empty($data["round_id"]))
            $model->setRoundId($data["round_id"]);
        $model->setCreatedAt($today);

        $this->_em->persist($model);
        if ($flush) {
            $this->_em->flush();
            return $model->getId();
        }

        return 0;
    }

    public function getLastBid($lotId)
    {
        $builder =  $this->createQueryBuilder('bid')
            ->andWhere('bid.lot_id = :lot_id')
            ->setParameter('lot_id', $lotId)
            ->orderBy('bid.price', 'DESC')
            ->getQuery();
        $result = $builder->getArrayResult();

        return (!empty($result[0])) ? $result[0] : [];
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function buyNowBid(array $data , bool $flush = true)
    {
        $today = $this->getDateTime();

        $model = new Bid();
        $model->setLotId($data["lot_id"]);
        $model->setProfileId($data["profile_id"]);
        $model->setPrice($data["bid_price"]);
        $model->setConfirmState(1);
        $model->setConfirmDate($today);
        if(!empty($data["round_id"]))
            $model->setRoundId($data["round_id"]);
        $model->setCreatedAt($today);

        $this->_em->persist($model);
        if ($flush) {
            $this->_em->flush();
            return $model->getId();
        }

        return 0;
    }

    // /**
    //  * @return Bid[] Returns an array of Bid objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bid
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
