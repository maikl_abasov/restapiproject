<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\TraitUtilsRepository;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class UserRepository extends ServiceEntityRepository
{
    use TraitUtilsRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add($data, bool $flush = true) : int
    {
        $today = date("Y-m-d H:i:s");

        $model = new User();
        $model->setEmail($data['email']);
        $model->setUsername($data['username']);
        $model->setPassword($data['password']);
        $model->setPhone($data['phone']);
        $model->setCreatedAt($today);

        $this->_em->persist($model);
        if ($flush) {
            $this->_em->flush();
            return $model->getId();
        }

        return 0;
    }

    public function convertToArray($items, $one = false)
    {
        $data = [];
        foreach ($items as $item) {
            $elem = [
                'id'         => $item->getId(),
                'username'   => $item->getUsername(),
                'email'      => $item->getEmail(),
                'password'   => $item->getPassword(),
                'phone'      => $item->getPhone(),
                'created_at' => $item->getCreatedAt(),
                'updated_at' => $item->getUpdatedAt(),
                'role'       => $item->getRole(),
                'active'     => $item->getActive(),
                'verify'     => $item->getVerify(),
            ];
            if($one) return $elem;
            $data[] = $elem;
        }
        return $data;
    }

    public function update($data)
    {
//        $model = new User();
//        $model->setEmail($data['email']);
//        $model->setUsername($data['username']);
//        $model->setPassword($data['password']);
//        $model->setPhone($data['phone']);
//
//        $this->_em->persist($model);
//        if ($flush) {
//            $this->_em->flush();
//            return $model->getId();
//        }
//        return 0;
    }

//    public function findByExampleField($value)
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult();
//    }


    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
