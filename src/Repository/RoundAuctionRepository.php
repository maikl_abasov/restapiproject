<?php

namespace App\Repository;

use App\Entity\Lot;
use App\Entity\RoundAuction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RoundAuction>
 *
 * @method RoundAuction|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoundAuction|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoundAuction[]    findAll()
 * @method RoundAuction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoundAuctionRepository extends ServiceEntityRepository
{
    use TraitUtilsRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoundAuction::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add($data, bool $flush = true) : int
    {
        $today = $this->getDateTime();

        $model = new RoundAuction();

        $model->setStartDate($data['start_date']);
        $model->setEndDate($data['end_date']);
        $model->setAuctionType($data['auction_type']);
        $model->setCreatedAt($today);

        $this->_em->persist($model);
        if ($flush) {
            $this->_em->flush();
            return $model->getId();
        }

        return 0;
    }

    public function getAllRounds($auctionType = 0, $limit = 0)
    {
        $builder = $this->createQueryBuilder('r');
        if($auctionType) {
            $builder = $builder->andWhere('r.auction_type = :type')
                               ->setParameter('type', $auctionType);
        }

        $builder = $builder->orderBy('r.start_date', 'DESC');

        if($limit) {
            $builder = $builder->setMaxResults($limit);
        }

        $builder = $builder->getQuery()->getArrayResult();

        return $builder;
    }

    public function getActiveRounds(int $auctionType, bool $orderByStart = true) : array {

        $orderByStart = $orderByStart ? 'ASC':'DESC';

        $query = "
            SELECT 
               round.id,
               round.start_date,
               round.end_date,
               round.auction_type,
               (SELECT count(lot.id) FROM App\Entity\Lot as lot 
                WHERE lot.round_id = round.id) AS lot_count
            FROM App\Entity\RoundAuction round
            WHERE round.start_date > :cur_date
            AND round.auction_type = :auction_type
            AND (SELECT count(l.id) FROM App\Entity\Lot as l 
                 WHERE l.round_id = round.id) > 0
            GROUP BY round.id     
            ORDER BY round.start_date {$orderByStart},
                     round.id {$orderByStart}
        ";

        $today = $this->todayDateModify('-1 day');

        $result = $this->getEntityManager()
                      ->createQuery($query)
                      ->setParameter('cur_date', $today)
                      ->setParameter('auction_type', $auctionType)
                      ->getArrayResult();
        return $result;
    }

}
