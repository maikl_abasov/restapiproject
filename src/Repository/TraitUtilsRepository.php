<?php


namespace App\Repository;

trait TraitUtilsRepository
{

    public function remove($param, $flush = true)
    {
        $item = $this->findOneBy($param);
        $this->_em->remove($item);
        if ($flush) {
            return $this->_em->flush();
        }
        return false;
    }

    public function convertToArray($items, $one = false)
    {
        $data = [];
        foreach ($items as $item) {
            $elem = $item->getPropsToArray();
            if($one) return $elem;
            $data[] = $elem;
        }
        return $data;
    }

    public function getDateTime()
    {
        $today = date("Y-m-d H:i:s");
        return $today;
    }

    public function prepareExec($query, $params = []) {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($query);
        $resultSet = $stmt->executeQuery($params);
        $result = $resultSet->fetchAllAssociative();
        return $result;
    }

    protected function todayDateModify($modify = '-1 day') {
        $today = date("Y-m-d");
        $date = \DateTime::createFromFormat("Y-m-d", $today);
        $date->modify($modify);
        $today = $date->format("Y-m-d");
        return $today;
    }
}