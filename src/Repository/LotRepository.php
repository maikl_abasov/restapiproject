<?php

namespace App\Repository;

use App\Entity\Lot;
use App\Entity\Car;
use App\Entity\Bid;
use App\Entity\RoundAuction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Lot>
 *
 * @method Lot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lot[]    findAll()
 * @method Lot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LotRepository extends ServiceEntityRepository
{
    use TraitUtilsRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lot::class);
    }


    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateStatus($data, $lotStatus) : int
    {
        $lotId = $data['lot_id'];
        $model = $this->find($lotId);
        $model->setStatus($lotStatus);
        $this->_em->persist($model);
        $this->_em->flush();
        return $model->getId();
    }


    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add($data, bool $flush = true) : int
    {
        $today = date("Y-m-d H:i:s");

        $lotStatus = (!empty($data['lot_status'])) ? $data['lot_status'] : 1;
        $profileId = (!empty($data['profile_id'])) ? $data['profile_id'] : 0;

        $model = new Lot();

        $model->setCarId($data['car_id']);
        $model->setRoundId($data['round_id']);
        $model->setAuctionType($data['auction_type']);
        $model->setCreatedAt($today);
        if($profileId)
            $model->setProfileId($profileId);
        $model->setStatus($lotStatus);

        $this->_em->persist($model);
        if ($flush) {
            $this->_em->flush();
            return $model->getId();
        }

        return 0;
    }

    public function getActiveAuctionCount($roundId)
    {
        $query = "
            SELECT COUNT(car.id) lot_count
            FROM App\Entity\Lot lot
            INNER JOIN App\Entity\Car car WITH (car.id = lot.car_id)
            WHERE lot.round_id = :round_id
        ";
        $result = $this->getEntityManager()
            ->createQuery($query)
            ->setParameter('round_id', $roundId)
            ->getSingleResult();
        return $result;
    }

    public function getActiveAuction($roundId, $formData = []) {

        $params = $where = [];
        $whereCondition = $limit = $orderBy = '';
        $limitSize = 5;
        $page = 1;
        $lotStatus = 1;

        if(!empty($formData)) {

            if(!empty($formData['mark_id'])) {
                $where[] = " mark.id = :mark_id ";
                $params['mark_id'] = $formData['mark_id'];
            }

            if(!empty($formData['model_id'])) {
                $where[] = " model.id = :model_id ";
                $params['model_id'] = $formData['model_id'];
            }

            if(!empty($formData['price_from'])) {
                $where[] = " car.start_price > :price_from ";
                $params['price_from'] = $formData['price_from'];
            }

            if(!empty($formData['price_to'])) {
                $where[] = " car.start_price < :price_to ";
                $params['price_to'] = $formData['price_to'];
            }

            if(!empty($formData['vin'])) {
                $where[] = " car.vin LIKE :vin ";
                $params['vin'] = '%' .$formData['vin'] . '%';
            }

            if(!empty($formData['lot_id'])) {
                $where[] = " lot.id LIKE :lot_id ";
                $params['lot_id'] = '%' .$formData['lot_id'] . '%';
            }

            if(!empty($formData['city_id'])) {
                $where[] = " car.city = :city_id";
                $params['city_id'] = $formData['city_id'];
            }

            if(isset($formData['nds']) && $formData['nds'] != -1) {
                $where[] = " car.nds = :nds ";
                $params['nds'] = $formData['nds'];
            }

            if(!empty($formData['page'])) {
                $page = $formData['page'];
            }

            if(!empty($formData['limit'])) {
                $limitSize = $formData['limit'];
            }

            if(!empty($formData['lot_status'])) {
                $lotStatus = $formData['lot_status'];
            }

            if(!empty($where)) {
                $whereCondition = ' AND ' .implode('AND', $where);
            }

            // dd($params);
        }

        $params['round_id'] = $roundId;

        $offset = ($page-1) * $limitSize;
        $limit = " LIMIT $offset, $limitSize ";

        $selectFields = "  
               car.*,    
                   
               lot.id lot_id,
               lot.auction_type,
               lot.status lot_status, 
               lot.round_id,

               round.start_date,
               round.end_date,   
                      
               city.city city_name,         
               mark.name mark_name,
               model.name model_name,
               generation.name generation_name,
               modification.name modification_name,
               modification.body_type,
               modification.fuel_type,
               modification.drive_type,    
               modification.transmission,
               modification.power,
               modification.engine_size
        ";


        $orderBy = " ORDER BY lot.id ASC ";

        $query = "
            FROM lot  
            INNER JOIN car ON (car.id = lot.car_id)
            LEFT JOIN `avc_marks` mark ON (mark.id = car.mark_id)
            LEFT JOIN `avc_models` model ON (model.id = car.model_id)
            LEFT JOIN `avc_generations` generation ON (generation.id = car.generation_id)
            LEFT JOIN `avc_modifications` modification ON (modification.id = car.modification_id)
            LEFT JOIN `geo_cities` city ON (city.city_id = car.city)
            LEFT JOIN `round_auction` round ON (round.id = lot.round_id)
            WHERE lot.round_id = :round_id
            AND lot.status = {$lotStatus}
            {$whereCondition}
        ";

        $queryAuctionList = "SELECT {$selectFields} {$query} {$orderBy} {$limit}";
        $queryAuctionCount = "SELECT COUNT(lot.id) {$query}";

        $lotsCount = $this->prepareExec($queryAuctionCount, $params);
        $result = $this->prepareExec($queryAuctionList, $params);

        $bidRepo = $this->getEntityManager()->getRepository(Bid::class);

        foreach ($result as $key => $lot) {
            $lotId = $lot['lot_id'];
            $lastBid = $bidRepo->getLastBid($lotId);
            $result[$key]['last_bid'] = $lastBid;
        }

        $lotsCount = (!empty($lotsCount[0]['COUNT(lot.id)'])) ? $lotsCount[0]['COUNT(lot.id)'] : 0;

        return [
            'list'  => $result,
            'count' => $lotsCount,
        ];
    }

}
